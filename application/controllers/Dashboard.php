<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$url="https://api.thingspeak.com/channels/1503718/feeds/last";
		$get_url = file_get_contents($url);
		$data_json = json_decode($get_url, true);
		$field1 = $data_json['field1'];
		$field2 = $data_json['field2'];
		$field3 = intval($data_json['field3']);
		$kadar_air = 100 - ($field3 / 1300 * 100);		
		$data['field1'] = $field1;
		$data['field2'] = $field2;
		$data['field3'] = $field3;
		$data['kadar_air'] = $kadar_air;


		//var_dump($field1); die;
		//$this->load->view('json/json_list',$data_array);
		$this->load->view('layouts/header');
		$this->load->view('layouts/sidebar');
		$this->load->view('dashboard', $data);
		$this->load->view('layouts/footer');
	}

	public function postData()
	{
		//API URL
		$url = 'https://api.thingspeak.com/update';

		

	}

	public function test()
	{
		$this->load->view('test');
	}
}
