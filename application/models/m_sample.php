<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_sample extends CI_Model{
	
	public function getAllSample()
	{
		$this->db->select('*');
		$this->db->from('tb_sample');
		$this->db->limit('100');
		$this->db->order_by('id_sample', 'DESC');
		return $this->db->get();
	}
	
	public function cekSample($tgl)
	{
		$this->db->select('*');
		$this->db->from('tb_sample');
		$this->db->where('tgl_update', $tgl);
		$data = $this->db->get();
		return $data->num_rows();
	}
	
	public function addSample($data)
	{
		$this->db->insert('tb_sample', $data);
	}
	
	public function dellSample($id)
	{
		$this->db->where($id);
        $this->db->delete('tb_sample');
		
	}
	
}