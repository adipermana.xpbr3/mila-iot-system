<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Begin Page Content -->
<div class="container-fluid mb-3">
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Table Data</h6>
        </div>
        <div class="card-body">
            <div class="d-sm-flex align-items-center justify-content-between mb-3">
				<a href="" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm" data-toggle="modal" data-target="#addSample">
					<i class="fas fa-plus fa-sm text-white-50"></i> Tambah Data</a>
                <a href="<?= base_url('generate') ?>" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
					<i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr class="text-center">
                                    <th>#</th>
                                    <th>Kelembaban Udara</th>
                                    <th>Suhu Udara</th>
                                    <th>Kadar Air Kacang</th>
                                    <th>Tgl Upload</th>
									<th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1; foreach($sample as $sample) :?>
                                
                                <tr id="<?= $sample['id_sample'] ?>" class="text-center">
                                    <td><?= $no++ ?></td>
                                    <td><?= number_format($sample['kelembaban'], 2, ',', '.')?></td>
                                    <td><?= number_format($sample['suhu'], 2, ',', ' ')?></td>
                                    <td><?= number_format($sample['kadar_air'], 2, ',', '.')?></td>
                                    <td><?= $sample['tgl_update']?></td>
									<td>
										<button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm remove">Hapus</button>
									</td>
                                </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Add sample modal -->
<div class="modal fade" id="addSample" tabindex="-1" aria-labelledby="addSampleLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addSampleLabel">Tambah Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="<?= base_url('add-sample') ?>">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Kelembaban Udara</label>
                        <input name="kelembaban" type="number" class="form-control" placeholder="Masukan nilai kelembaban..." autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label>Suhu Udara</label>
                        <input name="suhu" type="number" class="form-control" placeholder="Masukan nilai suhu..." autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label>Kadar Air</label>
                        <input name="air" type="number" class="form-control" placeholder="Masukan nilai kadar air..." autocomplete="off" required>
						<input name="source" type="hidden" class="form-control" value="1">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="d-none d-sm-inline-block btn btn-sm btn-secondary shadow-sm" data-dismiss="modal">Batal</button>
                    <button type="submit" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm tambah">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(".tambah").click(function(){
         Swal.fire(
		  'Berhasil!',
		  'Data telah ditambahkan.',
		  'success'
		);
    });
	
    $(".remove").click(function(){
        var id = $(this).parents("tr").attr("id");
    
       Swal.fire({
		  title: 'Data akan di hapus ?',
		  text: "Setelah di hapus data akan hilang.",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Ya, Hapus!',
		  cancelButtonText: 'Batal'
		}).then((result) => {
			if (result.isConfirmed) {
			  console.log(id);
			  $.ajax({
				 url: '<?= base_url('delete-sample/')?>'+id,
				 type: 'DELETE',
				 error: function() {
					Swal.fire(
						  'Deleted!',
						  'Your file has been deleted.1',
						  'warning'
						);
				 },
				 success: function(data) {
					  $("#"+id).remove();
					  Swal.fire(
						  'Berhasil!',
						  'Data telah di hapus.',
						  'success'
						);
				 }
			  });
			} else {
			  Swal.fire(
				  'Dibatalkan!',
				  'Data batal dihapus.',
				  'error'
				);
			}			
		 })
    });
    
</script>


