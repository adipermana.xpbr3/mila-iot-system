<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require('./application/third_party/phpoffice/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class DataSample extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_sample', 'sample');

		date_default_timezone_set('Asia/Jakarta');
	}

    public function index()
    {
        $data['sample'] = $this->sample->getAllSample()->result_array();
		//var_dump($data); die;
        $this->load->view('layouts/header');
        $this->load->view('layouts/sidebar');
        $this->load->view('dataSample', $data);
        $this->load->view('layouts/footer');
    }
	
	public function addSample()
	{
		$sts_source = $this->input->post('source');
		$tanggal = Date('d/m/Y, H:i:s');
		if($sts_source == '1'){
			$data = array(
				'kelembaban' => $this->input->post('kelembaban'),
				'suhu'     	 => $this->input->post('suhu'),
				'kadar_air'  => $this->input->post('air'),
				'tgl_update' => $tanggal,
			);
			$this->sample->addSample($data);
		} else {
			$tgl = $this->input->post('tanggal');
			$cek = $this->sample->cekSample($tgl);
			if($cek == 0){
				$data = array(
					'kelembaban' => $this->input->post('kelembaban'),
					'suhu'     	 => $this->input->post('suhu'),
					'kadar_air'  => $this->input->post('air'),
					'tgl_update' => $this->input->post('tanggal'),
				);
				$this->sample->addSample($data);
			}
		}
		redirect('data-sample');
	}
	
	public function dellSample($id)
	{
		$id = array('id_sample' => $id);
		$data = $this->sample->dellSample($id);
		echo 'Deleted successfully.';
		//redirect('data-sample');
	}

    public function export()
    {
		$data = $this->sample->getAllSample()->result_array();
        $spreadsheet = new Spreadsheet;

        $spreadsheet->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'No')
                    ->setCellValue('B1', 'Kelembaban Udara')
                    ->setCellValue('C1', 'Suhu Udara')
                    ->setCellValue('D1', 'Kadar Air')
                    ->setCellValue('E1', 'Tgl Upload');

        $kolom = 2;
        $nomor = 1;

        foreach($data as $data) {

            $spreadsheet->setActiveSheetIndex(0)
                        ->setCellValue('A' . $kolom, $nomor)
                        ->setCellValue('B' . $kolom, number_format($data['kelembaban'], 2, ',', ' '))
                        ->setCellValue('C' . $kolom, number_format($data['suhu'], 2, ',', ' '))
                        ->setCellValue('D' . $kolom, number_format($data['kadar_air'], 2, ',', ' '))
                        ->setCellValue('E' . $kolom, $data['tgl_update']);
            $nomor++;
            $kolom++;
        }

        $writer = new Xlsx($spreadsheet);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Sample.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');

    }
}