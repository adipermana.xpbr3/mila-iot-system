<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- Begin Page Content -->
<div class="container-fluid mb-3">

<!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
    </div>
    <div class="d-sm-flex align-items-center justify-content-end mb-3">
        <h5 class="h5 mb-0 font-weight-bold text-gray-800">Waktu Update : <span id="created">0</span></h5>
    </div>

    <div class="row">
        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                Kelembaban Udara</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><span id="kelembaban">0</span></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-tint fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                Suhu udara</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800"><span id="suhu">0</span><span> &deg;C</span></div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-temperature-low fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Kadar Air Kacang</div>
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto">
                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><span id="kadar_air">0</span></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Device</div>
                            <div class="row no-gutters align-items-center">
                                <div class="col-auto">
                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800"><span id="device">0</span></div> 
                                </div>
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-signal fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <!-- Area Chart -->
        <div class="col-xl-6 col-lg-6">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div
                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Data Kelembaban Udara</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-area text-center">
                    <iframe width="450" height="260" style="border: 1px solid #cccccc;" src="https://thingspeak.com/channels/1503718/charts/1?bgcolor=%23ffffff&color=%23d62020&dynamic=true&results=25&type=line&update=15"></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-lg-6">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div
                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Data Suhu Udara</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-area text-center">
                    <iframe width="450" height="260" style="border: 1px solid #cccccc;" src="https://thingspeak.com/channels/1503718/charts/2?bgcolor=%23ffffff&color=%23d62020&dynamic=true&results=25&type=line&update=15"></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6 col-lg-6">
            <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div
                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Data Kadar Air Kacang</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-area text-center">
                    <iframe width="450" height="260" style="border: 1px solid #cccccc;" src="https://thingspeak.com/channels/1503718/charts/3?bgcolor=%23ffffff&color=%23d62020&dynamic=true&results=25&type=line&update=15"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</div>
<script>
(function ambildata() {
    $.ajax({
        type: "GET",
        url: "https://api.thingspeak.com/channels/1503718/feeds/last",
        data: {},
        success:function(result){
            var json = JSON.parse(result);
            var field1 = Number(json['field1'])
            var field2 = Number(json['field2'])
            var field3 = Number(json['field3'])
            var created = new Date(json['created_at'])
            var time = new Date()
            var difference = time - created;
            var kadar_air = 100 - (field3 / 1300 * 100)
			/*Data Post*/
			var kelembaban = field1
			var suhu = field2
			var air = field3
			var tanggal = created.toLocaleString('en-GB')
			/*------------------------------------------------*/
			
            if(kadar_air <= 35 ) {
                var ket = "( Kering )"
            } else if( kadar_air > 35 && kadar_air <= 60) {
                var ket = "( Sedang )"
            } else {
                var ket = "( Basah )"
            }

            if(difference < 100000) {
                var device = "1 Perangkat Online"
            } else {
                var device = "Perangkat Offline"
            }

            document.getElementById('kelembaban').textContent = field1.toFixed(2) + ' %';
            document.getElementById('suhu').textContent = field2.toFixed(2);
            document.getElementById('kadar_air').textContent = field3 + '/' + kadar_air.toFixed(2) + ' % ' + ket;
            document.getElementById('device').textContent = device;
            document.getElementById('created').innerHTML = created.toLocaleString('en-GB');
            console.log(kelembaban,suhu,air,tanggal);
			
			$.ajax({
                type: "POST",
                url: "<?= base_url('add-sample')?>",
                data: {
                    kelembaban: kelembaban,
                    suhu: suhu,
                    air: air,
                    tanggal: tanggal,
					source : '0',
                },
                success: function() {                    
                    var status = "OK"
                    console.log(status);
                }

            });
        },
        complete: function() {
			setTimeout(ambildata, 5000);
		}
        
    }); 
})();
</script>


